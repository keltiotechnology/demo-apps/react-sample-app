import './App.css';
import React from 'react';
import axios from 'axios';
import keltioLogo from'./keltio-full.png';


const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:3000'

export default class App extends React.Component {
    state = {
        livres: [],

        titre: '',
        auteur: '',
        commentaires: ''
    }

    componentDidMount() {
        axios.get(`${API_URL}/livres`)
            .then(res => {
                const livres = res.data.data;
                this.setState({ livres });
            })
            .catch(err => {
                console.log('Fail')
                console.log(err)
            })
    }

    handleTitleChange = event => {
        this.setState({ titre: event.target.value });
    }
    handleAuthorChange = event => {
        this.setState({ auteur: event.target.value });
    }
    handleCommentChange = event => {
        this.setState({ commentaires: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

        const livre = {
            titre: this.state.titre,
            auteur: this.state.auteur,
            commentaires: this.state.commentaires
        };

        axios.post(`${API_URL}/livre`, livre)
            .then(res => {
                this.state.livres.push(res.data.data)
                this.setState({ livres: this.state.livres });
            })
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img  src={keltioLogo} className="keltio-logo" alt="fireSpot"/>


                    <div className="app-container">
                      <div>
                        <h1>Livres</h1>
                        {this.state.livres.map(livre => <div className="book-item" key={livre.livre_id}>{livre.titre} ({livre.auteur}): {livre.commentaires}</div>)}
                      </div>
                      <form onSubmit={this.handleSubmit} className="book-form">
                          <div className="input">
                              <label>
                                  Titre:
                              </label>
                              <input type="text" name="title" onChange={this.handleTitleChange} />
                          </div>
                          <div className="input">
                              <label>
                                  Auteur:
                              </label>
                              <input type="text" name="author" onChange={this.handleAuthorChange} />
                          </div>
                          <div className="input">
                              <label>
                                  Commentaires:
                              </label>
                              <input type="text" name="comment" onChange={this.handleCommentChange} />
                          </div>

                          <div className="button-container">
                              <button type="submit">Créer le livre</button>
                          </div>
                      </form>
                    </div>
                </header>
            </div>
        );
    }
}
